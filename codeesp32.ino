#include<iostream>
#include <Wire.h>
#include <SPI.h>
#include "DHT.h"
#include <Adafruit_Sensor.h>
#include "Adafruit_CCS811.h"
#include <WiFi.h>
#include <IFTTTWebhook.h>

/*const char consumer_key[] = "rpktSw6AaDFpR9X1SMmNMcAlS";
const char consumer_sec[] = "bzZVv4ot89WAME9mouWFZb1I4Ez0lVra5dOHLy39mPVf3am4sz";
const char accesstoken[] = "2320306573-FqQOrRITW9L0u1AP7MKk75itWZ8YbpLrP39joEx";
const char accesstoken_sec[] = "gHkwcqNAUDPCFetgQJZYSWdU7YpxXxDlCDUQYrBWpyQPV";*/

const char* host  = "http://86.196.62.11";//serveur d'hebergement du scrip php
const char* MY_SSID = "************";// ssid de connexion au wifi 
const char* MY_PWD = "************";// mdp de connexion au wifi
int LED_BUILTIN = 2;
int CoPin = 27;
int LumPin = 34;
float CoValue = 0;
float LumValue = 0;
int analog;
uint8_t DHTPin = 4;
DHT dht(DHTPin, DHT22);
float Temp;
float Humi;
float Co2;
String url;


Adafruit_CCS811 ccs;

WiFiClient client;

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    WiFi.begin(MY_SSID, MY_PWD);//connxion au wifi a l'aide des information de connexion
    Serial.begin(115200);//reglage du nombre de bauds 
    delay(100);
    pinMode(DHTPin, INPUT);
    dht.begin();

  ccs.begin();
  }

void loop() {


  delay (10000);//10000 en miliseconde donne 10 seconde

  CoValue = analogRead(CoPin)/*/1024*5.0*/;
    std::cout<<"\nle CO est de : "<<CoValue<<std::endl;

  Temp = dht.readTemperature();
    Humi = dht.readHumidity();
      std::cout<<"la temperature est de : "<<Temp<<std::endl;
        std::cout<<"l'humidité est de : "<<Humi<<std::endl;

  LumValue = analogRead(LumPin);
    std::cout<<"il y a "<<ConvertLux(LumValue)<<" lux"<<std::endl;

  ccs.readData();
    std::cout<<"le taux de co2 est de : "<<ccs.geteCO2()<<" ppm"<<std::endl;
      int Co2Value = ccs.geteCO2();

  if(WiFi.status() == WL_CONNECTED)//verification de la connexion wifi
    {
      digitalWrite(LED_BUILTIN, HIGH);

    if (client.connect(host, 80))//tentativ de connexion au serveur 
        {
    Serial.println("connected to server");
    //WiFi.printDiag(Serial);

      String url = "http://86.196.62.11/insert.php?Humi="
                   +  (String) Humi
                   +  "&Temp="  +(String) Temp
                   +  "&Co=" +(String) CoValue
                   +  "&Co2="    +(String) Co2Value
                   +  "&Lum="   +(String) LumValue;
                   Serial.println(url);//creation de la requete http avec les donnees recupere par les capteurs

      client.print(String("GET ") + url + " HTTP/1.1\r\n"
                         + "Host: " + host + "\r\n"
                         + "Connection: close\r\n\r\n");// envoy de la requete http au script php
                         unsigned long timeout = millis();
          while (client.available() == 0) {
         if (millis() - timeout > 5000) {
            Serial.println(">>> Client Timeout !");
            client.stop();
            return;
         }
         }
         while(client.available()) {
            String line = client.readStringUntil('\r');
            Serial.print(line);
         }

    Serial.println();
        Serial.println("closing connection");
            }
                else
                    {
                          Serial.println("connection to server failed");
                              }
                                        }

  else{
     Serial.println("connection to wifi lost");
        digitalWrite(LED_BUILTIN, LOW);
           WiFi.begin(MY_SSID, MY_PWD);
            }
             }

float ConvertLux(int raw)
{
  float Lux= raw * 5.0 / 4096;
    return pow (10,Lux);
    }